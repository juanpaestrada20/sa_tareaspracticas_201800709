import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import axios from 'axios';
import Swal from 'sweetalert2';

export default function LoginFormCalculadora() {
	const navigate = useNavigate();

	const [data, setData] = useState({
		nombre: '',
		carnet: '',
	});

	const handleData = (e) =>
		setData({ ...data, [e.target.name]: e.target.value });

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await axios.post(
				'http://localhost:8001/generar_token',
				data
			);
			if ([200, 201, 204].includes(response.status)) {
				localStorage.setItem('access_token', response.data.access_token)
				Swal.fire('Bienvenido!', '', 'success').then(
					() => {
						setData({ nombre: '', carnet: '' });
						navigate('/calculadora');
					}
				);
			} else {
				Swal.fire(response.data.message, '', 'error');
			}
		} catch (error) {
			handleError(error);
		}
	};

	const handleError = (error) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			Swal.fire(
				'Hubo un error',
				error.response.data?.length > 0
					? error.response.data
							.map((item) => `${item.field}fd ${item.message}`)
							.join(', ')
					: error.response.data.message,
				'error'
			);
			console.log(error.response.data);
			console.log(error.response.status);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			Swal.fire('Hubo un error', 'Intente realizarlo más tarde', 'error');
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
	};

	return (
		<Form onSubmit={handleSubmit} className='w-25'>
			<Row className='mb-3'>
				<Form.Group as={Col} controlId='nombre'>
					<Form.Label>Nombre:</Form.Label>
					<Form.Control
						type='text'
						placeholder='Ingrese su nombre'
						name='nombre'
						value={data.nombre}
						onChange={handleData}
					/>
				</Form.Group>
			</Row>

			<Row className='mb-3'>
				<Form.Group as={Col} controlId='carnet'>
					<Form.Label>Carnet:</Form.Label>
					<Form.Control
						type='text'
						placeholder='Ingrese su carnet'
						name='carnet'
						value={data.carnet}
						onChange={handleData}
					/>
				</Form.Group>
			</Row>

			<Button  variant='primary' type='submit'>
				Submit
			</Button>
		</Form>
	);
}
