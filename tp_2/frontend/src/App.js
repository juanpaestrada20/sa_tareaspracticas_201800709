import './App.css';
import { Routes, Route, Navigate } from "react-router-dom";
import NavbarContainer from './components/Navbar/NavbarContainer';
import Usuarios from './pages/Usuarios';
import Calculadora from './pages/Calculadora';
import Login from './pages/Login';
import LoginCalculadora from './pages/LoginCaluladora';

function App() {
  return (
    <div className="App">
      <NavbarContainer />
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/usuarios" element={<Usuarios />} />
        <Route path="/login_calculadora" element={<LoginCalculadora />} />
        <Route path="/calculadora" element={<Calculadora />} />
        <Route path="*" element={<Navigate to="/login" />} />
      </Routes>
    </div>
  );
}

export default App;
