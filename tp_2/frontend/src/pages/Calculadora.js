import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export default function Calculadora() {
	const access_token = localStorage.getItem('access_token');
	const [usuario, setUsuario] = useState({})

	const navigate = useNavigate();

	useEffect(() => {
		if (!access_token) {
			navigate('/login_calculadora');
		}
	}, [access_token, navigate]);
	const [operator, setOperator] = useState('add');
	const [data, setData] = useState({ num1: '', num2: '' });
	const [result, setResult] = useState('');
	const [validate, setValidate] = useState(false);
	const [secret, setSecret] = useState('');

	const handleData = (e) =>
		setData({ ...data, [e.target.name]: e.target.value });

	const handleSubmit = async (e) => {
		e.preventDefault();

		try {
			const response = await axios.post(
				`http://localhost:8001/operacion/${operator}`,
				data
			);
			if (200 === response.status) {
				setResult(response.data.result);
			} else {
				Swal.fire(response.data.message, '', 'error');
			}
		} catch (e) {
			handleError(e);
		}
	};

	const handleSubmitSecret = async (e) => {
		e.preventDefault();
		console.log(access_token)

		try {
			const response = await axios.post(
				`http://localhost:8001/verificar_token`,
				{secret: `sa_${secret}`},
				{headers:{Authorization: `Bearer ${access_token}`}}
			);
			if (200 === response.status && response.data?.result) {
				setUsuario(response.data.usuario)
				setValidate(response.data.result);
			} else {
				Swal.fire('Token inválido', '', 'error');
			}
		} catch (e) {
			handleError(e);
		}
	};

	const handleError = (error) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			Swal.fire('Hubo un error', error.response.data.message, 'error');
			console.log(error.response.data);
			console.log(error.response.status);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			Swal.fire('Hubo un error', 'Intente realizarlo más tarde', 'error');
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		//console.log(error.config);
	};
	if (!validate) {
		return (
			<Container>
				<Row className='my-4'>
					<Col className='p-3'>
						<h3 align='center'>Validar Secret</h3>
						<Form
							onSubmit={handleSubmitSecret}
							className=' d-flex justify-content-center w-100'>
							<Row className='align-items-center'>
								<Col xs='auto'>
									<Form.Control
										className='mb-2'
										id='secret'
										name='secret'
										placeholder='carnet'
										type='text'
										value={secret}
										onChange={(e) => setSecret(e.target.value)}
									/>
								</Col>
								<Col xs='auto'>
									<Button type='submit' className='mb-2'>
										Validar
									</Button>
								</Col>
							</Row>
						</Form>
					</Col>
				</Row>
			</Container>
		);
	} else {
		return (
			<Container>
				<Row className='my-4'>
					<Col className='p-3'>
						<h3 align='center'>Calculadora</h3>
							<Row className='justify-content-center align-items-center my-3'>
								<Col xs='auto'>Nombre: {usuario?.nombre}</Col>
								<Col xs='auto'>Carnet: {usuario?.carnet}</Col>
							</Row>
						<Form
							onSubmit={handleSubmit}
							className=' d-flex justify-content-center w-100'>
							<Row className='align-items-center'>
								<Col xs='auto'>
									<Form.Control
										className='mb-2'
										id='num1'
										name='num1'
										placeholder='Op 1'
										type='number'
										value={data.num1}
										onChange={handleData}
									/>
								</Col>
								<Col xs='auto'>
									<Form.Select
										value={operator}
										name='operator'
										className='mb-2'
										onChange={(e) =>
											setOperator(e.target.value)
										}>
										<option value={'add'}>+</option>
										<option value={'subtract'}>-</option>
										<option value={'multiply'}>*</option>
										<option value={'divide'}>/</option>
									</Form.Select>
								</Col>
								<Col xs='auto'>
									<Form.Control
										className='mb-2'
										id='num2'
										name='num2'
										placeholder='Op 2'
										type='number'
										value={data.num2}
										onChange={handleData}
									/>
								</Col>
								<Col xs='auto'>
									<Button type='submit' className='mb-2'>
										=
									</Button>
								</Col>
								<Col xs='auto'>
									<Form.Control
										className='mb-2'
										id='result'
										name='num2'
										placeholder='Result'
										type='number'
										value={result}
										disabled
									/>
								</Col>
							</Row>
						</Form>
					</Col>
				</Row>
				<Row className=' d-flex justify-content-center w-100'>
					<Button
						type='button'
						className='mb-2 w-25'
						onClick={() => {
							localStorage.removeItem('access_token');
							navigate('/login_calculadora');
						}}>
						Salir
					</Button>
				</Row>
			</Container>
		);
	}
}
