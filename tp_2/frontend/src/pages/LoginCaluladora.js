import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import LoginFormCalculadora from '../components/LoginFormCalculadora/LoginFormCalculadora';

export default function LoginCalculadora() {
	const access_token = localStorage.getItem('access_token');
	const navigate = useNavigate();

	useEffect(() => {
		if (access_token) navigate('/calculadora');
		return () => {};
	}, [access_token, navigate]);
	return (
		<div className='d-flex vw-100 mt-5 justify-content-center align-items-center'>
			<LoginFormCalculadora />
		</div>
	);
}
