import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import LoginForm from '../components/LoginForm/LoginForm';

export default function Login() {
	const usuario = localStorage.getItem('usuario');
	const navigate = useNavigate();

	useEffect(() => {
		if (usuario) navigate('/usuarios');
		return () => {};
	}, [usuario, navigate]);
	return (
		<div className='d-flex vw-100 mt-5 justify-content-center align-items-center'>
			<LoginForm />
		</div>
	);
}
