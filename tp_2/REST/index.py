from flask import Flask, request, jsonify
import requests
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='8000'
DEBUG=True

# Datos para la API gorest
API_URL = 'https://gorest.co.in/public/v1/users'
ACCESS_TOKEN = 'd5b59a9acd616a1bf864da38aeb0e8e7035f5a17ee21cd67c81a4b6e4feace5a'

# Definicion de headers por default que necesitan las peticiones
HEADERS = {'Authorization': 'Bearer {0}'.format(str(ACCESS_TOKEN))}

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

@app.route('/usuario', methods=['POST'])
def crear_usuario():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        ## Realizar peticion a API
        response_api = requests.post(API_URL, data=payload, headers=HEADERS)
        response_body = response_api.json()
        response_status_code = response_api.status_code

        # Se retorna la respuesta, status de la respuesta que regreso la API gorest
        return jsonify(response_body['data']), response_status_code, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/usuario/<int:id_usuario>', methods=['GET'])
def obtener_usuario(id_usuario):
    try:
        url = API_URL + '/{0}'.format(str(id_usuario))
        ## Realizar peticion a API
        response_api = requests.get(url, headers=HEADERS)
        response_body = response_api.json()
        response_status_code = response_api.status_code

        # Se retorna la respuesta, status de la respuesta que regreso la API gorest
        return jsonify(response_body['data']), response_status_code, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/usuario/<int:id_usuario>', methods=['PATCH', 'PUT'])
def actualizar_usuario(id_usuario):
    try:
        #Argregar id a url de API
        url = API_URL + '/{0}'.format(str(id_usuario))

        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        ## Realizar peticion a API
        response_api = requests.patch(url, data=payload, headers=HEADERS)
        response_body = response_api.json()
        response_status_code = response_api.status_code

        # Se retorna la respuesta, status de la respuesta que regreso la API gorest
        return jsonify(response_body['data']), response_status_code, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/usuario/<int:id_usuario>', methods=['DELETE'])
def eliminar_usuario(id_usuario):
    try:
        url = API_URL + '/{0}'.format(str(id_usuario))
        ## Realizar peticion a API
        response_api = requests.delete(url, headers=HEADERS)
        response_status_code = response_api.status_code

        # Se retorna la respuesta, status de la respuesta que regreso la API gorest
        return jsonify(), response_status_code, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/login', methods=['POST'])
def login():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)
        message = ''
        # Verificar que venga usuario y contraseña en el body
        if not 'usuario' in payload or payload['usuario'] == '':
            message += 'El campo usuario está faltante\n'
        if not 'contraseña' in payload or payload['contraseña'] == '':
            message += 'El campo contraseña esta faltante'

        # Si falta algún campo notificarle al usuario
        if len(message) > 0:
            return jsonify({"message": message}), 422, {'Content-Type': 'application/json'}

        # Se realiza la validación de usuario y contraseña
        if payload['usuario'] == 'JuanEstrada' and payload['contraseña'] == 'sa2022':
            return jsonify({"result": True, "message": "Bienvenido Juan Estrada"}), 200, {'Content-Type': 'application/json'}

        # Se retorna la respuesta de que el usuario y/o contraseña incorrecto
        return jsonify({"result": False, "message": "Usuario y/o contraseña incorrecta."}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)