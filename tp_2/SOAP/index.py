from email.mime import base
from flask import Flask, request, jsonify
import requests
import json
from flask_cors import CORS
import xmltodict
import base64
import hmac
import hashlib

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='8001'
DEBUG=True

# Datos para la el servicio SOAP de la calculadorea
SOAP_URL = 'http://www.dneonline.com/calculator.asmx'


# Datos para JWT
HEADER = {
    'alg': "HS256",
    'typ': "JWT"
}
SECRET = 'sa_201800709'

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

# Funcion para validar que vengan las unicas operaciones validas.
def validarOperacion(op):
    return op in ['add', 'subtract', 'multiply', 'divide']

# Funcion para obtener los headers a utilizar dependiendo de la operacion a realizar
def getHeaders(op):
    return {
    'Host': 'www.dneonline.com',
    'Content-Type': 'text/xml; charset=utf-8',
    'SOAPAction': "http://tempuri.org/{0}".format(op.capitalize())
    }

# Funcion para obtener xml a enviar dependiendo de la operacion a realizar y con los operandos.
def getXML(op, num1, num2):
    return """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
    <{0} xmlns="http://tempuri.org/">
        <intA>{1}</intA>
        <intB>{2}</intB>
    </{0}>
    </soap:Body>
</soap:Envelope>""".format(op.capitalize(), str(num1), str(num2))

# Funcion para obtener el resultado dependiendo operacion realizada
def getResult(op, result):
    if(op == 'add'):
        return result['AddResponse']['AddResult']
    elif(op == 'subtract'):
        return result['SubtractResponse']['SubtractResult']
    elif(op == 'multiply'):
        return result['MultiplyResponse']['MultiplyResult']
    elif(op == 'divide'):
        return result['DivideResponse']['DivideResult']
    else:
        return 0

@app.route('/operacion/<op>', methods=['POST'])
def realizar_operacion(op):
    try:
        # Validar que venga una de las operaciones validas
        if(not validarOperacion(op.lower())):
            return jsonify({"message": "Operación a realizar no válida, el servicio solo puede realizar Suma, Resta Multiplicación y División"}), 422, {'Content-Type': 'application/json'}

        # Convertir cuerpo de peticion a json
        payload = json.loads(request.data)

        # Validar que vengan los 2 operandos en la peticion
        if not ('num1' in payload and 'num2' in payload):
            return jsonify({"message": "Deben venir 2 operandos para poder realizar la operación"}), 422, {'Content-Type': 'application/json'}

        # Validar que los 2 operandos sean numeros enteros
        #if not (isinstance(payload['num1'], int) and isinstance(payload['num2'], int)):
        #    return jsonify({"message": "Los dos operandos deben ser números enteros."}), 422, {'Content-Type': 'application/json'}

        # Obtener XML y Headers para enviar en la peticion
        xml = getXML(op, payload['num1'], payload['num2'])
        headers = getHeaders(op)

        # Realizar peticion a servicio SOAP
        response_soap = requests.post(SOAP_URL, xml, headers=headers)

        # Ver si la peticion si fue realidada correctamente
        if response_soap.ok:
            # Transformar xlm a json
            response_json = xmltodict.parse(response_soap.text)

            return jsonify({"message": "Operación realizada con éxito", "result":getResult(op.lower(), response_json["soap:Envelope"]["soap:Body"])}), 200, {'Content-Type': 'application/json'}

        response_soap.raise_for_status()
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/generar_token', methods=['POST'])
def generar_token():
    try:
        #Convertimos el cuerpo de la petición a json
        payload = json.loads(request.data)

        # Verificamos que vengan los datos de carnet y nombre
        if not('carnet' in payload and 'nombre' in payload):
            return jsonify({"message": "Deben venir carnet y nombre para autenticarse"}), 422, {'Content-Type': 'application/json'}

        jwt = sign(payload, SECRET)

        return jsonify({'access_token': jwt}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/verificar_token', methods=['POST'])
def verificar_token():
    try:
        #Obtenemos el header de autorizacion
        auth_header = request.headers.get('Authorization')

        # Si no viene el token indicar error
        if(not auth_header):
            return jsonify({"message": "Debe autenticarse primero"}), 401, {'Content-Type': 'application/json'}

        # Obtenemos token de autorizacion
        token = auth_header.split(' ')[1]

        # separamos los datos publicos y la firma
        public_data = token.split('.')

        # Decodificamos la data y convertimos a json
        payload_jwt = json.loads(base64.b64decode(public_data[1].encode('utf-8')).decode('utf-8'))

        #Convertimos el cuerpo de la petición a json
        payload = json.loads(request.data)

        # Se veirifica que venga el secret en el body
        if( not 'secret' in payload):
            return jsonify({"message": "Debe enviar secret"}), 422, {'Content-Type': 'application/json'}

        # Firma de token con secret recibido
        jwt_nuevo = sign(payload_jwt, payload['secret'])

        # Validacion de jwt recibido con el generado
        correct_jwt = jwt_nuevo == token

        return jsonify({"result": correct_jwt, "usuario": payload_jwt}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(vars(e))
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

def sign(payload, secret):
    header = base64.b64encode(json.dumps(HEADER).encode('utf-8')).decode("utf-8")
    data = base64.b64encode(json.dumps(payload).encode('utf-8')).decode('utf-8')
    msg = header + '.' + data
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(msg, 'utf-8'), hashlib.sha256).hexdigest()
    token = msg + '.' + signature
    return token

if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)