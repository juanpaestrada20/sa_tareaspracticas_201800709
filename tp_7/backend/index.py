from flask import Flask, request, jsonify
import json
from flask_cors import CORS
from flask_mysqldb import MySQL
from dotenv import dotenv_values

env = dotenv_values(".env")

app = Flask(__name__)
CORS(app, origins=["*"])

# Informacion de MySQL
app.config['MYSQL_HOST'] = env['MYSQL_HOST']
app.config['MYSQL_USER'] = env['MYSQL_USER']
app.config['MYSQL_PASSWORD'] = env['MYSQL_PASSWORD']
app.config['MYSQL_DB'] = env['MYSQL_DB']
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['MYSQL_PORT'] = int(env['MYSQL_PORT'])
app.config['MYSQL_UNIX_SOCKET'] = env['MYSQL_PORT']

#Conexion a mysql
mysql = MySQL(app)

# Variables para indicar como correr el servidor
HOST='0.0.0.0'
PORT='8000'
DEBUG=True

def print_message(message):
    print("======================================================================")
    print(message)
    print("======================================================================")

def execute(sql, params):
    try:
        cursor = mysql.connection.cursor()
        cursor.execute(sql, params)
        print(cursor.__dict__)
        mysql.connection.commit()
        return {
            'id': cursor.lastrowid,
            'results': cursor.fetchall()
        }
    except Exception as e:
        mysql.connection.rollback()
        print(e)
        return False

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

# Ruta para crear usuario
@app.route('/usuario', methods=['POST'])
def crear_usuario():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('nombre' in payload and 'carnet' in payload):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Generación de comando SQL
        sql = "INSERT INTO usuario (nombre, carnet) VALUES (%s, %s)"
        val = (payload['nombre'],str(payload['carnet']))

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "usuario": result['id'], "message": "Usuario registrado"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Usuario no se pudo registrar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

def obtener_usuario(id_usuario):
    try:
        # Petición recibida
        print_message(id_usuario)

        # Generación de comando SQL
        sql = "SELECT * FROM usuario  WHERE id_usuario = %s"
        val = (id_usuario,)

        result = execute(sql, val)
        if(result):
            return {"usuario": result['results'][0]}
        else:
            return False
    except Exception as e:
        print(e)
        return False

#Ruta para obtener todos los usuarios
@app.route('/usuario', methods=['GET'])
def obtener_usuarios():
    try:
        # Generación de comando SQL
        sql = "SELECT * FROM usuario"

        result = execute(sql,())
        if(result):
            return jsonify({"success": True, "usuarios": result['results']}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Usuarios no encontrados"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


@app.route('/usuario', methods=['PUT'])
def actualizar_usuario():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('id_usuario' in payload and 'nombre' in payload and 'carnet' in payload):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Verificar que exista el usuario
        usuario = obtener_usuario(payload['id_usuario'])

        if not usuario or len(usuario) == 0:
            print_message("Usuario no encontrado para actualizar")
            return jsonify({"message": "Usuario no encontrado para actualizar", "success": False}), 200, {'Content-Type': 'application/json'}


        # Generación de comando SQL
        sql = "UPDATE usuario SET nombre = %s, carnet = %s WHERE id_usuario = %s"
        val = (payload['nombre'],payload['carnet'], payload['id_usuario'])

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "message": "Usuario actualizado"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Usuario no se pudo actualizar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/usuario/<id_usuario>', methods=['DELETE'])
def eliminar_usuario(id_usuario):
    try:
        # Petición recibida
        print_message(id_usuario)

        # Verificar que exista la usuario
        usuario = obtener_usuario(id_usuario)

        if not usuario or len(usuario) == 0:
            print_message("Usuario no encontrada para elimnar")
            return jsonify({"message": "Usuario no encontrada para elimnar", "success": False}), 200, {'Content-Type': 'application/json'}


        # Generación de comando SQL
        sql = "DELETE FROM usuario WHERE id_usuario = %s"
        val = (id_usuario,)

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "message": "Usuario eliminado"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Usuario no se pudo eliminar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)