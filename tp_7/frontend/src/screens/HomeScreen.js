import React, { useState } from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';

import MainLayout from '../layouts/MainLayout';

import Formulario from '../components/Formulario';
import StudentTable from '../components/StudentTable';

export default function HomeScreen() {
	const [data, setData] = useState({ carnet: '', nombre: '' });
	return (
		<MainLayout>
			<div className='my-5'>
				<Row>
					<Col sm={12} lg={6}>
						<Formulario data={data} setData={setData} />
					</Col>
					<Col sm={12} lg={6}>
						<StudentTable setData={setData} />
					</Col>
				</Row>
			</div>
		</MainLayout>
	);
}
