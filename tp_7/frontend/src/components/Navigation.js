import React from 'react';
import { Container, Navbar } from 'react-bootstrap';

export default function Navigation() {
	return (
		<Navbar bg='dark' variant='dark'>
			<Container>
				<Navbar.Brand href='/'>TP - 7</Navbar.Brand>
			</Container>
		</Navbar>
	);
}
