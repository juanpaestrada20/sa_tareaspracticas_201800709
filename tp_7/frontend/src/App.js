import React from 'react';
import { Link, Route, Routes } from 'react-router-dom';

import './App.css';
import HomeScreen from './screens/HomeScreen';

function App() {
	return (
		<div className='App'>
			<Routes>
				<Route index path='/' element={<HomeScreen />} />
				<Route
					path='*'
					element={
						<main className='d-flex w-100 h-100 justify-content-center align-items-center flex-column'>
							<h1>Página no encontrada!</h1>
							<Link to={'/'}>Volver al inicio</Link>
						</main>
					}
				/>
			</Routes>
		</div>
	);
}

export default App;
