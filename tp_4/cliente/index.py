from flask import Flask, request, jsonify
import requests
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='8000'
DEBUG=True

# Datos para microservicio de restaurante y repartidor
API_ESB = 'http://localhost:3000'

def print_message(message):
    print("======================================================================")
    print(message)
    print("======================================================================")

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

@app.route('/cliente/pedido', methods=['POST'])
def crear_pedido():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga nombre de cliente y menu
        if not ('cliente' in payload and 'menu' in payload):
            print_message("Debe venir cliente y menu a pedir")
            return jsonify({"message": "Debe venir cliente y menu a pedir"}), 422, {'Content-Type': 'application/json'}

        # Mostramos la informacion del pedido y cliente
        print_message("El cliente {0} ha realizado un pedido de {1}".format(payload['cliente'], payload['menu']))

        # Notificar al restaurante del pedido
        response_restaurante = requests.post(API_ESB + '/restaurante/pedido', data=json.dumps(payload))
        response_body = response_restaurante.json()

        # Verificar que el restaurante si haya almacenado el pedido
        if response_body['success'] == True:
            print_message("El pedido ha sido notificado al restaurante puede consultarlo con el id: {0}".format(response_body['pedido']['id_pedido']))
            return jsonify({"success": True, "message": "El pedido ha sido notificado al restaurante puede consultarlo con el id: {0}".format(response_body['pedido']['id_pedido'])}), 200, {'Content-Type': 'application/json'}
        else:
            print_message("El pedido no ha sido recibido por el restaurante, inténtelo más tarde")
            return jsonify({"success": False, "message": "El pedido no ha sido recibido por el restaurante, inténtelo más tarde"}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/cliente/restaurante/<int:id_pedido>', methods=['GET'])
def restaurante_estado(id_pedido):
    try:
        # Mostramos la informacion del pedido y cliente
        print_message("El cliente ha solicitado informacion de su pedido {0} en el restaurante".format(id_pedido))

        # Notificar al restaurante del pedido
        response_restaurante = requests.get(API_ESB + '/restaurante/pedido/{0}'.format(id_pedido))
        response_body = response_restaurante.json()

        # Verificar que el restaurante si haya almacenado el pedido
        if response_body['success'] == True:
            print_message("El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, response_body['pedido']['estado_pedido']))
            return jsonify({"success": True, "message": "El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, response_body['pedido']['estado_pedido'])}), 200, {'Content-Type': 'application/json'}
        else:
            print_message( "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/cliente/repartidor/<int:id_pedido>', methods=['GET'])
def repartidor_estado(id_pedido):
    try:
        # Mostramos la informacion del pedido y cliente
        print_message("El cliente ha solicitado información de su pedido {0} al repartidor".format(id_pedido))

        # Notificar al restaurante del pedido
        response_repartidor = requests.get(API_ESB + '/repartidor/pedido/{0}'.format(id_pedido))
        response_body = response_repartidor.json()

        # Verificar que el restaurante si haya almacenado el pedido
        if response_body['success'] == True:
            print_message("El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, response_body['pedido']['estado_pedido']))
            return jsonify({"success": True, "message": "El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, response_body['pedido']['estado_pedido'])}), 200, {'Content-Type': 'application/json'}
        else:
            print_message( "El pedido: {0} no ha sido encontrado con un repartidor, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado con un repartidor, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

def suscribirse_ESB():
    try:
        contrato = {
            "baseURL": "http://localhost:8000",
            "rutas": [
                {
                    "ruta": "/cliente/pedido",
                    "metodo": "POST"
                },
                {
                    "ruta": "/cliente/restaurante/:id",
                    "metodo": "GET"
                },
                {
                    "ruta": "/cliente/repartidor/:id",
                    "metodo": "GET"
                }
            ]
        }
        # Notificar al restaurante del pedido
        respose_ESB = requests.post(API_ESB + '/suscribirse', data=json.dumps(contrato))
        response_body = respose_ESB.json()
        #print(response_body)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    suscribirse_ESB()
    app.run(HOST, PORT, DEBUG)