from flask import Flask, request, jsonify
import requests
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='8001'
DEBUG=True

# Datos para microservicio de restaurante y repartidor
API_ESB = 'http://localhost:3000'

# Array de pedidos
Pedidos = []
contador = 1

def print_message(message):
    print("======================================================================")
    print(message)
    print("======================================================================")

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

@app.route('/restaurante/pedido', methods=['POST'])
def crear_pedido():
    global contador, Pedidos
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga nombre de cliente y menu
        if not ('cliente' in payload and 'menu' in payload):
            print_message("Debe venir cliente y menu a pedir")
            return jsonify({"message": "Debe venir cliente y menu a pedir"}), 422, {'Content-Type': 'application/json'}

        # Mostramos la informacion del pedido y cliente
        print_message("Se ha recibida un pedido de {0} del cliente {1}".format(payload['menu'], payload['cliente']))

        # Guardar informacion de pedido
        data = {
            "id_pedido": contador,
            "cliente": payload['cliente'],
            "menu": payload['menu'],
            "estado_pedido": "Recibido en restaurante"
        }
        Pedidos.append(data)
        contador += 1

        print_message("El pedido ha sido almacenado al restaurante puede consultarlo o actualizarlo con el id: {0}".format(data["id_pedido"]))
        return jsonify({"success": True, "pedido": data, "message": "El pedido ha sido notificado al restaurante puede consultarlo o actualizarlo con el id: {0}".format(data['id_pedido'])}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/restaurante/pedido/<int:id_pedido>', methods=['GET'])
def pedido_estado(id_pedido):
    global Pedidos
    try:
        # Mostramos la informacion del pedido y cliente
        print_message("El cliente ha solicitado información de su pedido {0} en el restaurante".format(id_pedido))

        # Buscar pedido
        pedido = list(filter(lambda item: int(item['id_pedido']) == int(id_pedido), Pedidos))

        # Verificar que el restaurante si haya encontrado el pedido
        if len(pedido) > 0:
            print_message("El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, pedido[0]['estado_pedido']))
            return jsonify({"success": True, "pedido": pedido[0], "message": "El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, pedido[0]['estado_pedido'])}), 200, {'Content-Type': 'application/json'}
        else:
            print_message( "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/restaurante/pedido/<int:id_pedido>', methods=['PUT', 'PATCH', 'POST'])
def avisar_repartidor(id_pedido):
    try:
        # Buscar pedido y actualizarlo si se encuentra
        pedido = list(filter(lambda item: int(item['id_pedido']) == int(id_pedido), Pedidos))

        # Si no encontramos el pedido notificamos error
        if len(pedido) == 0:
            print_message( "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}

        # Actualizamos registro
        index = Pedidos.index(pedido[0])
        pedido[0]['estado_pedido'] = "Enviado con Repartidor"
        Pedidos[index] = pedido[0]

        # Notificar al repartidor del pedido
        response_repartidor = requests.post(API_ESB + '/repartidor/pedido', data=json.dumps(pedido[0]))
        response_body = response_repartidor.json()

        # Verificar que el restaurante si haya almacenado el pedido
        if response_body['success'] == True:
            print_message("El pedido id: {0} fue en enviado con un repartidor".format(id_pedido))
            return jsonify({"success": False, "pedido": response_body['pedido'], "message": "El pedido id: {0} fue en enviado con un repartidor".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
        else:
            print_message( "El pedido: {0} no ha sido encontrado con un repartidor, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado con un repartidor, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

def suscribirse_ESB():
    try:
        contrato = {
            "baseURL": "http://localhost:8001",
            "rutas": [
                {
                    "ruta": "/restaurante/pedido",
                    "metodo": "POST"
                },
                {
                    "ruta": "/restaurante/pedido/:id",
                    "metodo": "GET"
                },
                {
                    "ruta": "/restaurante/pedido/:id",
                    "metodo": "PUT"
                }
            ]
        }
        # Notificar al restaurante del pedido
        respose_ESB = requests.post(API_ESB + '/suscribirse', data=json.dumps(contrato))
        response_body = respose_ESB.json()
        print(response_body)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    suscribirse_ESB()
    app.run(HOST, PORT, DEBUG)