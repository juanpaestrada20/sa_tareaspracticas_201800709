from flask import Flask, request, jsonify
from requests import Request, Session
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='3000'
DEBUG=True

Suscripciones = []
s=Session()

def print_message(message):
    print("======================================================================")
    print(message)
    print("======================================================================")

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

@app.route("/suscribirse", methods=['POST'])
def suscribirse():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)
        # Verificamos que venga baseURL y ls rutas
        if not ('baseURL' in payload and 'rutas' in payload):
            print_message("Debe indicar la url y las rutas que suscribira al ESB")
            return jsonify({"message": "Debe indicar la url y las rutas que suscribira al ESB"}), 422, {'Content-Type': 'application/json'}

        # Verificamos que vengan las rutas en formato de arreglo
        if not type(payload['rutas']) == list:
            print_message("Debe proporcionar las rutas a suscribir")
            return jsonify({"message": "Debe proporcionar las rutas a suscribir"}), 422, {'Content-Type': 'application/json'}

        # Verificamos que si ya esta suscrito
        if len(list(filter(lambda suscpripcion: suscpripcion['baseURL'] == payload['baseURL'], Suscripciones))) > 0:
            print_message("El servicio ya se encuentra suscrito en ESB")
            return jsonify({"message": "El servicio ya se encuentra suscrito en ESB"}), 200, {'Content-Type': 'application/json'}

        # Realizamos la suscripción
        for ruta in payload['rutas']:
            print_message("Ruta Nueva\nURL: {0} - MÉTODO: {1} - RUTA: {2}".format(payload['baseURL'], ruta['metodo'], ruta['ruta']))
            Suscripciones.append({
                "baseURL": payload['baseURL'],
                "metodo": ruta['metodo'],
                "ruta": ruta['ruta'],
            })

        # Notificar suscripcion exitosa
        print_message("Se ha suscrito exitosamente el servicio")
        return jsonify({"success": True, "message": "Se ha suscrito exitosamente el servicio"}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/suscripciones', methods=['GET'])
def suscripciones():
    return jsonify({'suscripciones': Suscripciones}), 200, {'Content-Type': 'application/json'}

@app.route('/<path:ruta>', methods=['POST', 'GET', 'PUT'])
@app.route('/<path:ruta>/<int:id_pedido>', methods=['POST', 'GET', 'PUT'])
def consultar_ruta(ruta, id_pedido = 0):
    try:
        method = request.method
        path = '/' + ruta if id_pedido == 0 else '/' + ruta + '/:id'

        rutas_metodo = list(filter(lambda ruta: ruta['metodo'] == method and ruta['ruta'] == path, Suscripciones))
        if len(rutas_metodo) == 0:
            print_message("Ruta no encontrada en ESB")
            return jsonify({"message": "Ruta no encontrada en ESB"}), 200, {'Content-Type': 'application/json'}
        ruta = ''
        if id_pedido == 0:
            ruta = rutas_metodo[0]['baseURL']+ path
        else:
            ruta = rutas_metodo[0]['baseURL']+ path.replace(":id", str(id_pedido))

        print_message("Se realizara la peticion a {0}".format(ruta))
        req = Request(method, data=request.data, url=ruta)
        prepped = s.prepare_request(req)
        res = s.send(prepped)

        return jsonify(res.json()), res.status_code, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)