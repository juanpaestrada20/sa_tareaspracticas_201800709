from flask import Flask, request, jsonify
import requests
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='8002'
DEBUG=True

# Datos para microservicio de restaurante y repartidor
API_ESB = 'http://localhost:3000'

Pedidos = []

def print_message(message):
    print("======================================================================")
    print(message)
    print("======================================================================")

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

@app.route('/repartidor/pedido', methods=['POST'])
def recibir_pedido():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga nombre de cliente y menu
        if not ('cliente' in payload and 'menu' in payload):
            print_message("Debe venir cliente y menu a pedir")
            return jsonify({"message": "Debe venir cliente y menu a pedir"}), 422, {'Content-Type': 'application/json'}

        # Mostramos la informacion del pedido y cliente
        print_message("El restaurante le ha indicado que entregue el pedido: {0} al cliente: {1}".format(payload['menu'], payload['cliente']))

        # Almacenamos pedido a entregar
        Pedidos.append(payload)

        print_message("El pedido ha sido almacenado al repartidor para entregar puede consultarlo o actualizarlo con el id: {0}".format(payload["id_pedido"]))
        return jsonify({"success": True, "pedido": payload, "message": "El pedido ha sido notificado al repartidor para entregar puede consultarlo o actualizarlo con el id: {0}".format(payload['id_pedido'])}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/repartidor/pedido/<int:id_pedido>', methods=['GET'])
def restaurante_estado(id_pedido):
    global Pedidos
    try:
        # Mostramos la informacion del pedido y cliente
        print_message("El cliente ha solicitado información de su pedido {0} al repartidor".format(id_pedido))

        # Buscar pedido
        pedido = list(filter(lambda item: int(item['id_pedido']) == int(id_pedido), Pedidos))

        # Verificar que el restaurante si haya encontrado el pedido
        if len(pedido) > 0:
            print_message("El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, pedido[0]['estado_pedido']))
            return jsonify({"success": True, "pedido": pedido[0], "message": "El pedido id: {0} se encuentra en el estado: {1}".format(id_pedido, pedido[0]['estado_pedido'])}), 200, {'Content-Type': 'application/json'}
        else:
            print_message( "El pedido: {0} no ha sido encontrado con el repartidor, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado con el repartidor, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/repartidor/pedido/<int:id_pedido>', methods=['PUT', 'PATCH'])
def entregar_pedido(id_pedido):
    try:
        # Buscar pedido y actualizarlo si se encuentra
        pedido = list(filter(lambda item: int(item['id_pedido']) == int(id_pedido), Pedidos))

        # Si no encontramos el pedido notificamos error
        if len(pedido) == 0:
            print_message( "El pedido: {0} no ha sido encontrado con el repartidor, inténtelo más tarde".format(id_pedido))
            return jsonify({"success": False, "message": "El pedido: {0} no ha sido encontrado en el restaurante, inténtelo más tarde".format(id_pedido)}), 200, {'Content-Type': 'application/json'}

        # Actualizamos registro
        index = Pedidos.index(pedido[0])
        pedido[0]['estado_pedido'] = "Entregado"
        Pedidos[index] = pedido[0]

        print_message("El pedido id: {0} fue entregado al cliente".format(id_pedido))
        return jsonify({"success": False, "pedido": pedido[0], "message": "El pedido id: {0} fue entregado al cliente".format(id_pedido)}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

def suscribirse_ESB():
    try:
        contrato = {
            "baseURL": "http://localhost:8002",
            "rutas": [
                {
                    "ruta": "/repartidor/pedido",
                    "metodo": "POST"
                },
                {
                    "ruta": "/repartidor/pedido/:id",
                    "metodo": "GET"
                },
                {
                    "ruta": "/repartidor/pedido/:id",
                    "metodo": "PUT"
                }
            ]
        }
        # Notificar al restaurante del pedido
        respose_ESB = requests.post(API_ESB + '/suscribirse', data=json.dumps(contrato))
        response_body = respose_ESB.json()
        print(response_body)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    suscribirse_ESB()
    app.run(HOST, PORT, DEBUG)