import React, { useState } from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Formulario from '../components/Formulario/Formulario';
import axios from 'axios';
import Swal from 'sweetalert2';

export default function Usuarios() {
    const [method, setMethod] = useState('');
    const [id,setID] = useState('');

    const handleSubmit =  (e) => {
        e.preventDefault();
        if(id.trim() === '') return;
        setMethod('update');
    }

    const handleDelete = () => {
        if(!id) return;
        axios.delete(`http://localhost:8000/usuario/${id}`).then(res=>{
            if(res.status===204){
                Swal.fire('Usuario eliminado!', `Se eliminó el usuario con ID:${id}`, 'success');
                setID('')
                setMethod('')
            }
        })
    }
	return (
		<Container>
			<Row className='my-4'>
				<Col className='p-3'>
					<h3 align='center'>Usuario</h3>
					<Formulario id={id} method={method} setMethod={setMethod} setID={setID} />
				</Col>
				<Col className='p-3'>
					<h3 align='center'>Buscar Usuario</h3>
					<Form onSubmit={handleSubmit}>
						<Row className='align-items-center'>
							<Col className='my-2'>
								<Form.Control
									id='inlineFormInputName'
									placeholder='Buscar por id'
                                    value={id}
                                    onChange={e=>setID(e.target.value)}
								/>
							</Col>
							<Col xs='auto' className='my-1'>
								<Button type='submit'>Buscar</Button>
							</Col>
						</Row>
					</Form>
                    {id && method === 'update' && (<Button type='button' variant="danger" onClick={()=>handleDelete()}>Eliminar</Button>)}
				</Col>
			</Row>
		</Container>
	);
}
