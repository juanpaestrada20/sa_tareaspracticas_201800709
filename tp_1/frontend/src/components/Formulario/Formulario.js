import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import axios from 'axios';
import Swal from 'sweetalert2';

export default function Formulario({ method, id, setMethod, setID }) {
	const [data, setData] = useState({
		name: '',
		email: '',
		gender: '',
		status: '',
	});

	const handleData = (e) =>
		setData({ ...data, [e.target.name]: e.target.value });

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			if (method !== 'update') {
				const response = await axios.post(
					'http://localhost:8000/usuario',
					data
				);
				if ([200, 201, 204].includes(response.status)) {
					Swal.fire(
						'Usuario Creado!',
						`Usuario Creado con ID: ${response.data.id}`,
						'success'
					);
					setData({ name: '', email: '', gender: '', status: '' });
					setID('');
				} else {
					Swal.fire(response.data.message, '', 'error');
				}
			} else {
				if (!data?.id) return;
				const response = await axios.patch(
					`http://localhost:8000/usuario/${data?.id}`,
					data
				);
				if ([200, 201, 204, 304].includes(response.status)) {
					Swal.fire(
						'Usuario Actualizado!',
						`Usuario Actualizado con ID: ${response.data.id}`,
						'success'
					);
					setData({ name: '', email: '', gender: '', status: '' });
					setMethod('');
					setID('');
				} else {
					Swal.fire(response.data.message, '', 'error');
				}
			}
		} catch (error) {
			handleError(error);
		}
	};

	const handleError = (error) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			Swal.fire(
				'Hubo un error',
				error.response.data?.length > 0 ?
					error.response.data.map((item) => `${item.field}fd ${item.message}`)
					.join(', ') : error.response.data.message,
				'error'
			);
			console.log(error.response.data);
			console.log(error.response.status);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			Swal.fire('Hubo un error', 'Intente realizarlo más tarde', 'error');
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		//console.log(error.config);
	};

	useEffect(() => {
		if (method === 'update' && id) {
			axios
				.get(`http://localhost:8000/usuario/${id}`)
				.then((res) => {
					if (res.status === 200) {
						setData({ ...res.data });
					}
				})
				.catch(handleError);
		}
	}, [method, id, setID, setData]);

	return (
		<Form onSubmit={handleSubmit}>
			<Row className='mb-3'>
				<Form.Group as={Col} controlId='nombre'>
					<Form.Label>Nombre:</Form.Label>
					<Form.Control
						type='text'
						placeholder='Ingrese su nombre'
						name='name'
						value={data.name}
						onChange={handleData}
					/>
				</Form.Group>

				<Form.Group as={Col} controlId='email'>
					<Form.Label>Email:</Form.Label>
					<Form.Control
						type='email'
						placeholder='Ingrese su correo'
						name='email'
						value={data.email}
						onChange={handleData}
					/>
				</Form.Group>
			</Row>
			<Row className='mb-3'>
				<Form.Group as={Col} controlId='formGridState'>
					<Form.Label>Género:</Form.Label>
					<Form.Select
						value={data.gender}
						name='gender'
						onChange={handleData}>
						<option value={''}>Seleccione uno</option>
						<option value={'male'}>Másculino</option>
						<option value={'female'}>Femenino</option>
					</Form.Select>
				</Form.Group>

				<Form.Group as={Col} controlId='formGridState'>
					<Form.Label>Status</Form.Label>
					<Form.Select
						value={data.status}
						name='status'
						onChange={handleData}>
						<option value={''}>Seleccione uno</option>
						<option value={'active'}>Activo</option>
						<option value={'inactive'}>Inactivo</option>
					</Form.Select>
				</Form.Group>
			</Row>

			<Form.Group className='mb-3' id='formGridCheckbox'>
				<Form.Check
					type='checkbox'
					label='Actualizar'
					name='method'
					checked={method === 'update'}
					onChange={(e) =>
						setMethod(e.target.checked ? 'update' : '')
					}
				/>
			</Form.Group>

			<Button variant='primary' type='submit'>
				Submit
			</Button>
		</Form>
	);
}
