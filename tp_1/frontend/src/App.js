import './App.css';
import { Routes, Route, Navigate } from "react-router-dom";
import NavbarContainer from './components/Navbar/NavbarContainer';
import Usuarios from './pages/Usuarios';
import Calculadora from './pages/Calculadora';

function App() {
  return (
    <div className="App">
      <NavbarContainer />
      <Routes>
        <Route path="/usuarios" element={<Usuarios />} />
        <Route path="/calculadora" element={<Calculadora />} />
        <Route path="*" element={<Navigate to="/usuarios" />} />
      </Routes>
    </div>
  );
}

export default App;
