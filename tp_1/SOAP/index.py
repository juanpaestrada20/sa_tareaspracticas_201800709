from flask import Flask, request, jsonify
import requests
import json
from flask_cors import CORS
import xmltodict

app = Flask(__name__)
CORS(app, origins=["*"])

# Variables para indicar como correr el servidor
HOST='localhost'
PORT='8001'
DEBUG=True

# Datos para la el servicio SOAP de la calculadorea
SOAP_URL = 'http://www.dneonline.com/calculator.asmx'

@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

# Funcion para validar que vengan las unicas operaciones validas.
def validarOperacion(op):
    return op in ['add', 'subtract', 'multiply', 'divide']

# Funcion para obtener los headers a utilizar dependiendo de la operacion a realizar
def getHeaders(op):
    return {
    'Host': 'www.dneonline.com',
    'Content-Type': 'text/xml; charset=utf-8',
    'SOAPAction': "http://tempuri.org/{0}".format(op.capitalize())
    }

# Funcion para obtener xml a enviar dependiendo de la operacion a realizar y con los operandos.
def getXML(op, num1, num2):
    return """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
    <{0} xmlns="http://tempuri.org/">
        <intA>{1}</intA>
        <intB>{2}</intB>
    </{0}>
    </soap:Body>
</soap:Envelope>""".format(op.capitalize(), str(num1), str(num2))

# Funcion para obtener el resultado dependiendo operacion realizada
def getResult(op, result):
    if(op == 'add'):
        return result['AddResponse']['AddResult']
    elif(op == 'subtract'):
        return result['SubtractResponse']['SubtractResult']
    elif(op == 'multiply'):
        return result['MultiplyResponse']['MultiplyResult']
    elif(op == 'divide'):
        return result['DivideResponse']['DivideResult']
    else:
        return 0

@app.route('/operacion/<op>', methods=['POST'])
def realizar_operacion(op):
    try:
        # Validar que venga una de las operaciones validas
        if(not validarOperacion(op.lower())):
            return jsonify({"message": "Operación a realizar no válida, el servicio solo puede realizar Suma, Resta Multiplicación y División"}), 422, {'Content-Type': 'application/json'}

        # Convertir cuerpo de peticion a json
        payload = json.loads(request.data)

        # Validar que vengan los 2 operandos en la peticion
        if not ('num1' in payload and 'num2' in payload):
            return jsonify({"message": "Deben venir 2 operandos para poder realizar la operación"}), 422, {'Content-Type': 'application/json'}

        # Validar que los 2 operandos sean numeros enteros
        #if not (isinstance(payload['num1'], int) and isinstance(payload['num2'], int)):
        #    return jsonify({"message": "Los dos operandos deben ser números enteros."}), 422, {'Content-Type': 'application/json'}

        # Obtener XML y Headers para enviar en la peticion
        xml = getXML(op, payload['num1'], payload['num2'])
        headers = getHeaders(op)

        # Realizar peticion a servicio SOAP
        response_soap = requests.post(SOAP_URL, xml, headers=headers)

        # Ver si la peticion si fue realidada correctamente
        if response_soap.ok:
            # Transformar xlm a json
            response_json = xmltodict.parse(response_soap.text)

            return jsonify({"message": "Operación realizada con éxito", "result":getResult(op.lower(), response_json["soap:Envelope"]["soap:Body"])}), 200, {'Content-Type': 'application/json'}

        response_soap.raise_for_status()
    except Exception as e:
        print(e)
        return jsonify({"message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)