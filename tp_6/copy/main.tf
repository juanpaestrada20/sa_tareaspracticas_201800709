provider "google" {
  credentials = file("credentials.json")

  project = "sa-g1-343004"
  region  = "us-central1"
  zone    = "us-central1-a"
}

# google_compute_firewall.default:
resource "google_compute_firewall" "default" {
    direction               = "INGRESS"
    disabled                = false
    name                    = "tp6-201800709-3000-22-copy"
    network                 = "https://www.googleapis.com/compute/v1/projects/sa-g1-343004/global/networks/default"
    priority                = 1000
    project                 = "sa-g1-343004"
    source_tags             = []

    allow {
        ports    = [
            "3000", "5000",
            "22",
        ]
        protocol = "tcp"
    }

    timeouts {}
}

# google_compute_instance.default:
resource "google_compute_instance" "default" {
    deletion_protection  = false
    enable_display       = false
    guest_accelerator    = []
    labels               = {}
    machine_type         = "e2-medium"
    metadata             = {
        "ssh-key"        = <<-EOT
            juanpa200799:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDnqKeWkao6SdtYqME0FX/HUTmPl3UaCEYaKqzlsKTdU+psrTe7Tm2YMOBAUU3LZPJ4u9DMumbY2wwphUcr66RcF5+ktM08L6RWBQrNtl3uFidHCc6/Za374EuXvShtjOcl0X+Hir+eiJmzKq7jAhWNDpWmc9+m2yomV6J+eMDi7udBpRk2DoxCyn/SrzHgoJ4afp3Ysouczcu18HVNeg3O6hr6RiTCcRzF3igyUjb6DTrEeOU+J9L9VFLzjkhB/S0FGANql8CM+ZIETwWF3CQ3ieL0QvmzCML0qBsHmz8+ZEtBLcOd9ClMm5NEyy7jhXHX91px4y8ifECxj+VWzH3PxwakwlxjrpkurXSrv8S+NvSOWl2zKiw8DR3cz07njzweNVjybjdpW4MZo06dUUXZFPZxNaLW8KhLSFdwmOP4wuwi4iWv5vwUoEDBxA+3kf39dvvMCjo+65tf7eKOMM4CHdcWNTciqZWBoJ4IqxUeyOXxZWfQbFIxDYAkq7OmFGs= juanpa200799
        EOT
        "startup-script" = "sudo apt-get update; sudo apt-get dist-upgrade -y; curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -; sudo apt-get update; sudo apt install nodejs; cd /home/juanpa200799; git clone https://juanpaestrada20:Juanpaest20@gitlab.com/juanpaestrada20/basic_server.git; cd basic_server; sudo npm install; sudo node index.js"
    }
    name                 = "vm1-tp6-201800709-edcf9e281bdf19b1-copy"
    project              = "sa-g1-343004"
    resource_policies    = []
    tags                 = []
    zone                 = "us-central1-a"

    boot_disk {
        auto_delete = true
        device_name = "persistent-disk-0"
        mode        = "READ_WRITE"

        initialize_params {
            image  = "https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-v20220308"
            labels = {}
            size   = 10
            type   = "pd-standard"
        }
    }

    network_interface {
        network            = "https://www.googleapis.com/compute/v1/projects/sa-g1-343004/global/networks/default"
        queue_count        = 0
        stack_type         = "IPV4_ONLY"
        subnetwork         = "https://www.googleapis.com/compute/v1/projects/sa-g1-343004/regions/us-central1/subnetworks/default"
        subnetwork_project = "sa-g1-343004"

        access_config {
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
    }

    shielded_instance_config {
        enable_integrity_monitoring = true
        enable_secure_boot          = false
        enable_vtpm                 = true
    }

    timeouts {}
}
