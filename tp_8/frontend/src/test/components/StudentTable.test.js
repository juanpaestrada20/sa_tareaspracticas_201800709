/* eslint-disable testing-library/prefer-screen-queries */
/* eslint-disable testing-library/render-result-naming-convention */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, waitFor } from '@testing-library/react';
import StudentTable from '../../components/StudentTable';
import axios from 'axios';

jest.mock('axios');
afterEach(() => {
	jest.resetAllMocks();
});
afterAll(() => {
	jest.clearAllMocks();
});

test('se renderiza el componente', async () => {
	const setData = jest.fn();
	axios.get.mockResolvedValueOnce({
		data: {
			success: true,
			usuarios: [{ id_usuario: 1, nombre: 'Juan', carnet: '201800709' }],
		},
	});
	const component = render(<StudentTable setData={setData} />);
	expect(component.container).toBeInTheDocument();
	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);

	await waitFor(() => {
		expect(component.getByText('201800709')).toBeInTheDocument();
	});
});

test('Realizar get de registros', async () => {
	const setData = jest.fn();
	axios.get.mockResolvedValueOnce({
		data: { success: false, message: 'Error al obtener datos' },
	});
	const component = render(<StudentTable setData={setData} />);

	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);

	await waitFor(() => {
		expect(component.queryByText('201800709')).not.toBeInTheDocument();
	});
});

test('realizar get con error en response', async () => {
	const setData = jest.fn();
	axios.get.mockRejectedValueOnce({
		response: { success: false, message: 'Error al obtener datos' },
	});
	const component = render(<StudentTable setData={setData} />);

	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);

	await waitFor(() => {
		expect(component.queryByText('201800709')).not.toBeInTheDocument();
	});
});

test('realizar con error en request', async () => {
	const setData = jest.fn();
	axios.get.mockRejectedValueOnce({
		request: { success: false, message: 'Error al guardar' },
	});
	const component = render(<StudentTable setData={setData} />);

	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);

	await waitFor(() => {
		expect(component.queryByText('201800709')).not.toBeInTheDocument();
	});
});

test('realizar get con error a parte', async () => {
	const setData = jest.fn();
	axios.get.mockRejectedValueOnce({
		success: false,
		message: 'Error al guardar',
	});
	const component = render(<StudentTable setData={setData} />);

	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);

	await waitFor(() => {
		expect(component.queryByText('201800709')).not.toBeInTheDocument();
	});
});

test('Actualizar resgistro', async () => {
	const setData = jest.fn();
	axios.get.mockResolvedValueOnce({
		data: {
			success: true,
			usuarios: [{ id_usuario: 1, nombre: 'Juan', carnet: '201800709' }],
		},
	});
	const component = render(<StudentTable setData={setData} />);
	expect(component.container).toBeInTheDocument();
	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);

	await waitFor(() => {
		expect(component.getByText('201800709')).toBeInTheDocument();
	});

	const btnActualizar = component.getByTestId('btn-actualizar');
	fireEvent.click(btnActualizar);

	expect(setData).toHaveBeenCalledTimes(1);
});

test('Eliminar resgistro correcto', async () => {
	const setData = jest.fn();
	window.scrollTo = jest.fn();
	axios.get.mockResolvedValueOnce({
		data: {
			success: true,
			usuarios: [{ id_usuario: 1, nombre: 'Juan', carnet: '201800709' }],
		},
	});
	const component = render(<StudentTable setData={setData} />);
	expect(component.container).toBeInTheDocument();
	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);
	await waitFor(() => {
		expect(component.getByText('201800709')).toBeInTheDocument();
	});

	const btnEliminar = component.getByTestId('btn-eliminar');
	fireEvent.click(btnEliminar);

	let btnEliminar2;

	await waitFor(() => {
		expect(component.getByText('Eliminar')).toBeInTheDocument();
		btnEliminar2 = component.getByText('Eliminar');
	});

	axios.delete.mockResolvedValueOnce({
		data: {
			success: true,
			message: 'Registro eliminado correctamente',
		},
	});
	fireEvent.click(btnEliminar2);
});

test('Eliminar resgistro con error en respuesta', async () => {
	const setData = jest.fn();
	window.scrollTo = jest.fn();
	axios.get.mockResolvedValueOnce({
		data: {
			success: true,
			usuarios: [{ id_usuario: 1, nombre: 'Juan', carnet: '201800709' }],
		},
	});
	const component = render(<StudentTable setData={setData} />);
	expect(component.container).toBeInTheDocument();
	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);
	await waitFor(() => {
		expect(component.getByText('201800709')).toBeInTheDocument();
	});

	const btnEliminar = component.getByTestId('btn-eliminar');
	fireEvent.click(btnEliminar);

	let btnEliminar2;

	await waitFor(() => {
		expect(component.getByText('Eliminar')).toBeInTheDocument();
		btnEliminar2 = component.getByText('Eliminar');
	});

	axios.delete.mockResolvedValueOnce({
		data: {
			success: false,
			message: 'Registro sin poder sido eliminado',
		},
	});
	fireEvent.click(btnEliminar2);
});

test('Eliminar resgistro con error', async () => {
	const setData = jest.fn();
	window.scrollTo = jest.fn();
	axios.get.mockResolvedValueOnce({
		data: {
			success: true,
			usuarios: [{ id_usuario: 1, nombre: 'Juan', carnet: '201800709' }],
		},
	});
	const component = render(<StudentTable setData={setData} />);
	expect(component.container).toBeInTheDocument();
	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(
		`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
	);
	await waitFor(() => {
		expect(component.getByText('201800709')).toBeInTheDocument();
	});

	const btnEliminar = component.getByTestId('btn-eliminar');
	fireEvent.click(btnEliminar);

	let btnEliminar2;

	await waitFor(() => {
		expect(component.getByText('Eliminar')).toBeInTheDocument();
		btnEliminar2 = component.getByText('Eliminar');
	});

	axios.delete.mockRejectedValueOnce({
		success: false,
		message: 'Registro sin poder sido eliminado',
	});
	fireEvent.click(btnEliminar2);
});
