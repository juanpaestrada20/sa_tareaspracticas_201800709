/* eslint-disable testing-library/prefer-screen-queries */
/* eslint-disable testing-library/render-result-naming-convention */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import Formulario from '../../components/Formulario';
import axios from 'axios';

jest.mock('axios');

test('se renderiza el componente', () => {
	const component = render(<Formulario />);
	expect(component.container).toBeInTheDocument();
});

test('limpiar formulario', () => {
	const data = {
		carnet: '201800709',
		nombre: 'Juan',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);
	expect(component.getByPlaceholderText('No. Carnet').value).toBe(
		data.carnet
	);
	expect(component.getByPlaceholderText('Nombre').value).toBe(data.nombre);

	const button = component.getByText('Limpiar');
	fireEvent.click(button);
	component.rerender(<Formulario data={data} setData={setData} />);

	expect(component.getByPlaceholderText('No. Carnet').value).toBe('');
	expect(component.getByPlaceholderText('Nombre').value).toBe('');
});

test('enviar formulario con error en respuesta', () => {
	const data = {
		carnet: '',
		nombre: '',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);
	const inputCarnet = component.getByPlaceholderText('No. Carnet');
	const inputNombre = component.getByPlaceholderText('Nombre');

	setData({
		carnet: (Math.random() * 10000).toFixed(0).toString(),
		nombre: 'Prueba Front',
	});
	fireEvent.change(inputCarnet, { target: { value: data.carnet } });
	fireEvent.change(inputNombre, { target: { value: data.nombre } });
	component.rerender(<Formulario data={data} setData={setData} />);

	axios.post.mockResolvedValueOnce({data: {success: false, message: 'Error al guardar'}});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect(axios.post).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});

test('enviar formulario correcto', () => {
	const data = {
		carnet: '',
		nombre: '',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);
	const inputCarnet = component.getByPlaceholderText('No. Carnet');
	const inputNombre = component.getByPlaceholderText('Nombre');

	setData({
		carnet: (Math.random() * 10000).toFixed(0).toString(),
		nombre: 'Prueba Front',
	});
	fireEvent.change(inputCarnet, { target: { value: data.carnet } });
	fireEvent.change(inputNombre, { target: { value: data.nombre } });
	component.rerender(<Formulario data={data} setData={setData} />);

	axios.post.mockResolvedValueOnce({data: {success: true, message: 'Usuario guardado'}});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect(axios.post).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});

test('enviar formulario con error en response', () => {
	const data = {
		carnet: '',
		nombre: '',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);
	const inputCarnet = component.getByPlaceholderText('No. Carnet');
	const inputNombre = component.getByPlaceholderText('Nombre');

	setData({
		carnet: (Math.random() * 10000).toFixed(0).toString(),
		nombre: 'Prueba Front',
	});
	fireEvent.change(inputCarnet, { target: { value: data.carnet } });
	fireEvent.change(inputNombre, { target: { value: data.nombre } });
	component.rerender(<Formulario data={data} setData={setData} />);

	axios.post.mockRejectedValueOnce({response: {success: false, message: 'Error al guardar'}});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect(axios.post).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});

test('enviar formulario con error en request', () => {
	const data = {
		carnet: '',
		nombre: '',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);
	const inputCarnet = component.getByPlaceholderText('No. Carnet');
	const inputNombre = component.getByPlaceholderText('Nombre');

	setData({
		carnet: (Math.random() * 10000).toFixed(0).toString(),
		nombre: 'Prueba Front',
	});
	fireEvent.change(inputCarnet, { target: { value: data.carnet } });
	fireEvent.change(inputNombre, { target: { value: data.nombre } });
	component.rerender(<Formulario data={data} setData={setData} />);

	axios.post.mockRejectedValueOnce({request: {success: false, message: 'Error al guardar'}});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect(axios.post).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});

test('enviar formulario con error a parte', () => {
	const data = {
		carnet: '',
		nombre: '',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);
	const inputCarnet = component.getByPlaceholderText('No. Carnet');
	const inputNombre = component.getByPlaceholderText('Nombre');

	setData({
		carnet: (Math.random() * 10000).toFixed(0).toString(),
		nombre: 'Prueba Front',
	});
	fireEvent.change(inputCarnet, { target: { value: data.carnet } });
	fireEvent.change(inputNombre, { target: { value: data.nombre } });
	component.rerender(<Formulario data={data} setData={setData} />);

	axios.post.mockRejectedValueOnce({success: false, message: 'Error al guardar'});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect(axios.post).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});

test('enviar formulario de actualizacion con error en respuesta', () => {
	const data = {
		id_usuario: 1,
		carnet: '201800709',
		nombre: 'Prueba Fron',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);

	axios.put.mockResolvedValueOnce({data: {success: false, message: 'Error al guardar'}});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.put).toHaveBeenCalledTimes(1);
	expect(axios.put).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});

test('enviar formulario de actualizacion correcto', () => {
	const data = {
		id_usuario: 1,
		carnet: '201800709',
		nombre: 'Prueb Front',
	};
	const setData = (values) => {
		data.carnet = values.carnet;
		data.nombre = values.nombre;
	};
	const component = render(<Formulario data={data} setData={setData} />);

	axios.put.mockResolvedValueOnce({data: {success: true, message: 'Usuario guardado'}});

	const button = component.getByText('Guardar');
	fireEvent.click(button);

	expect(axios.put).toHaveBeenCalledTimes(1);
	expect(axios.put).toHaveBeenCalledWith(`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`, data);
});