/* eslint-disable testing-library/prefer-screen-queries */
/* eslint-disable testing-library/render-result-naming-convention */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import {  render } from '@testing-library/react';
import Navigation from '../../components/Navigation';

test ('se renderiza el componente', () => {
    const component = render(<Navigation />);
    expect(component.container).toBeInTheDocument();
});