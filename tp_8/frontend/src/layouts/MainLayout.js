import React from 'react'
import { Container } from 'react-bootstrap'
import Navigation from '../components/Navigation'

export default function MainLayout(props) {
  return (
    <>
        <Navigation />
        <Container>
            {props.children}
        </Container>
    </>
  )
}
