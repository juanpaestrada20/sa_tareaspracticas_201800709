import React, { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import axios from 'axios';

import { FaEdit, FaTrash } from 'react-icons/fa';

export default function StudentTable(props) {
	const [students, setStudents] = useState([]);

	const deleteUser = async (id_usuario) => {
		const result = await Swal.fire({
			title: '¿Seguro que desea eliminar?',
			text: 'Esta acción no se pueder revertir!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Eliminar',
		});
		if (result.isConfirmed) {
			deleteUserAxios(id_usuario);
		}
	};

	const deleteUserAxios = async (id_usuario) => {
		try {
			const { data } = await axios.delete(
				`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario/${id_usuario}`
			);
			if (data.success) {
				Swal.fire('Eliminado!', data.message, 'success').then(() =>
					window.location.reload()
				);
			} else Swal.fire('Error!', data.message, 'error');
		} catch (error) {
			handleError(error);
		}
	};

	const handleError = (error) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			Swal.fire('Error', error.response.message, 'error');
			//console.log(error.response);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			Swal.fire('Error', 'Error en la petición', 'error');
			//console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			Swal.fire('Error', error.message, 'error');
			//console.log('Error', error.message);
		}
		//console.log(error.config);
	};

	useEffect(() => {
		async function fetchData() {
			try {
				const { data } = await axios.get(
					`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`
				);
				if (data.success) setStudents(data.usuarios);
				else {
					setStudents([]);
					Swal.fire('Error!', data.message, 'error');
				}
			} catch (error) {
				handleError(error);
			}
		}
		fetchData();
		return () => setStudents([]);
	}, []);

	return (
		<Table striped bordered hover variant='dark'>
			<thead>
				<tr>
					<th>#</th>
					<th>Carnet</th>
					<th>Nombre</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{students &&
					students.length > 0 &&
					students.map((student, i) => (
						<tr key={student.id_usuario} data-testid='list-row'>
							<td>{i + 1}</td>
							<td>{student.carnet}</td>
							<td>{student.nombre}</td>
							<td className='d-flex justify-content-center align-items-center'>
								<Button
									variant='warning'
									className='d-flex justify-content-center align-items-center me-2 py-2'
									onClick={() =>
										props.setData({ ...student })
									}
									data-testid="btn-actualizar">
									<FaEdit />
								</Button>
								<Button
									variant='danger'
									className='d-flex justify-content-center align-items-center py-2'
									onClick={() =>
										deleteUser(student.id_usuario)
									}
									data-testid="btn-eliminar">
									<FaTrash />
								</Button>
							</td>
						</tr>
					))}
			</tbody>
		</Table>
	);
}
