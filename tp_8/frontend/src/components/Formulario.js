import React from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

import Logo from '../images/Usac_logo.png';

export default function Formulario(props) {
	const changeData = (e) =>
		props.setData({ ...props?.data, [e.target.name]: e.target.value });

	const handleError = (error) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			//console.log(error.response);
			Swal.fire('Error', error.response.message, 'error');
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			Swal.fire('Error', 'Error en la petición', 'error');
			//console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			Swal.fire('Error', error.message, 'error');
			//console.log('Error', error.message);
		}
		//console.log(error.config);
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			if (props?.data?.id_usuario) {
				const { data } = await axios.put(
					`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`,
					{ ...props.data }
				);
				if (data.success)
					Swal.fire('Actualizado!', data.message, 'success').then(
						() => window.location.reload()
					);
				else Swal.fire('Error!', data.message, 'error');
			} else {
				const { data } = await axios.post(
					`${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/usuario`,
					{ ...props.data }
				);
				if (data.success)
					Swal.fire('Guardado!', data.message, 'success').then(() =>
						window.location.reload()
					);
				else Swal.fire('Error!', data.message, 'error');
			}
		} catch (error) {
			handleError(error);
		}
	};

	return (
		<div className='d-flex w-100 h-auto justify-content-center px-3'>
			<Form
				className='border border-1 rounded p-5 w-100'
				onSubmit={handleSubmit}>
				<div className='d-flex w-100 justify-content-center align-items-center flex-column'>
					<h4 className='pb-3'>Datos del Estudiante</h4>
					<img
						className='mb-3'
						src={Logo}
						alt='Logo USAC'
						height={150}
					/>
				</div>
				<Form.Group className='mb-3' controlId='formCarnet'>
					<Form.Label>Carnet</Form.Label>
					<Form.Control
						type='text'
						name='carnet'
						placeholder='No. Carnet'
						value={props?.data?.carnet}
						onChange={changeData}
					/>
				</Form.Group>

				<Form.Group className='mb-3' controlId='formName'>
					<Form.Label>Nombre</Form.Label>
					<Form.Control
						type='text'
						name='nombre'
						placeholder='Nombre'
						value={props?.data?.nombre}
						onChange={changeData}
					/>
				</Form.Group>
				<div className='d-flex justify-content-evenly'>
					<Button variant='primary' type='submit'>
						Guardar
					</Button>
					<Button
						variant='danger'
						onClick={() =>
							props.setData({ carnet: '', nombre: '' })
						}>
						Limpiar
					</Button>
				</div>
			</Form>
		</div>
	);
}
