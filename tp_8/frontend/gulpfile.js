const gulp = require('gulp');
const zip = require('gulp-zip');

gulp.task('default', function () {
	// place code for your default task here
	return gulp
		.src([
			'./public/**',
			'./src/**/*',
			'./node_modules/**/*',
			'./.env',
			'!./src/test',
			'!./coverage',
			'!./.dockerfile',
			'!./gulpfile.js',
			'!./.gitignore',
			'!./Dockerfile',
			'!./**.json',
			'!./README.md',
		], { base: '.' })
		.pipe(zip('frontend.zip'))
		.pipe(gulp.dest('../artifacts'));
});
