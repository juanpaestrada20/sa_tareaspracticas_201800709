const gulp = require('gulp');
const zip = require('gulp-zip');

gulp.task('default', function () {
	// place code for your default task here
	return gulp
		.src([
			'./app/**',
			'./.env',
			'./run.py',
			'./requirements.txt',
			'!./app/__pycache__/',
		], { base: '.' })
		.pipe(zip('backend.zip'))
		.pipe(gulp.dest('../artifacts'));
});
