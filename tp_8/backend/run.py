from app import app

# Variables para indicar como correr el servidor
HOST='0.0.0.0'
PORT='8000'
DEBUG=True

if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)