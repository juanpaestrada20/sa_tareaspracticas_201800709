from app import app
import random

def test_service():
    '''
    DADO servidor corriendo
    CUANDO se hace una peticion get a la ruta /
    ENTONCES comprueba que el servidor se encuentre corriendo
    '''
    response = app.test_client().get('/')

    assert response.status_code == 200

def test_crear_usuario():
    '''
    DADO un modelo de Usuario
    CUANDO se crea un nuevo usuario
    ENTONCES comprueba que los campos nombre y carnet están definidos correctamente
    '''
    response = app.test_client().post('/usuario', json={
        "nombre": "Usuario Prueba",
        "carnet": int(random.random()*1000)
    })

    assert response.status_code == 200
    assert response.json.get("success")

def test_crear_usuario_duplicado():
    '''
    DADO un modelo de Usuario
    CUANDO se crea un nuevo usuario
    ENTONCES comprueba que los campos nombre y carnet están definidos correctamente y el usuario ya exista
    '''
    response = app.test_client().post('/usuario', json={
        "nombre": "Usuario Prueba",
        "carnet": "201800709"
    })

    assert response.status_code == 400
    assert not response.json.get("success")

def test_crear_usuario_incompleto():
    '''
    DADO un modelo de Usuario
    CUANDO se crea un nuevo usuario
    ENTONCES comprueba que los campos nombre y carnet no están definidos correctamente
    '''
    response = app.test_client().post('/usuario', json={
        "nombre": "Juan Estrada",
    })

    assert response.status_code == 422
    assert not response.json.get("success")

def test_obtener_usuarios():
    '''
    DADO un modelo de Usuario
    CUANDO se obtiene una lista de usuario
    ENTONCES comprueba que si venga una lista
    '''
    response = app.test_client().get('/usuario')

    assert response.status_code == 200
    assert isinstance(response.json.get("usuarios"), list)

def test_actualizar_usuario():
    '''
    DADO un modelo de Usuario
    CUANDO se actualiza un usuario
    ENTONCES comprueba que los campos id_usuario nombre y carnet están definidos correctamente y el usuario ya exista
    '''
    response = app.test_client().put('/usuario', json={
        "carnet": random.random(),
        "id_usuario": 5,
        "nombre": "Usuario Actualización"
    })

    assert response.status_code == 200
    assert response.json.get("success")

def test_actualizar_usuario_incompleto():
    '''
    DADO un modelo de Usuario
    CUANDO se actualiza un usuario
    ENTONCES comprueba que los campos id_usuario, nombre y carnet no están definidos correctamente 
    '''
    response = app.test_client().put('/usuario', json={
        "id_usuario": 5,
        "nombre": "Usuario Actualización"
    })

    assert response.status_code == 422
    assert not response.json.get("success")

def test_actualizar_usuario_inexistente():
    '''
    DADO un modelo de Usuario
    CUANDO se actualiza un usuario
    ENTONCES comprueba que los campos nombre y carnet están definidos correctamente y no exista el usuario
    '''
    response = app.test_client().put('/usuario', json={
        "id_usuario": 1,
        "carnet": "123456",
        "nombre": "Usuario Actualización"
    })

    assert response.status_code == 200
    assert not response.json.get("success")

def test_actualizar_usuario_incongruente():
    '''
    DADO un modelo de Usuario
    CUANDO se actualiza un usuario
    ENTONCES comprueba que los campos nombre y carnet están definidos correctamente y no actualice
    '''
    response = app.test_client().put('/usuario', json={
        "id_usuario": 5,
        "carnet": "12345678910111213141516",
        "nombre": "Usuario de Prueba"
    })

    assert response.status_code == 400
    assert not response.json.get("success")

def test_usuario_usuario_inexistente():
    '''
    DADO un modelo de Usuario
    CUANDO se elimina un usuario
    ENTONCES comprueba que los campos id_usuario no están definidos correctamente y no elimine
    '''
    response = app.test_client().delete('/usuario/1')

    assert response.status_code == 200
    assert not response.json.get("success")

def test_usuario_usuario():
    '''
    DADO un modelo de Usuario
    CUANDO se elimina un usuario
    ENTONCES comprueba que los campos id_usuario están definidos correctamente y lo elimine
    '''
    usuario = app.test_client().post('/usuario', json={
        "nombre": "Usuario Prueba",
        "carnet": "prueba"
    })

    id = usuario.json.get("usuario")

    response = app.test_client().delete('/usuario/{}'.format(id))

    assert response.status_code == 200
    assert response.json.get("success")