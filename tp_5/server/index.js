const express = require('express')
const app = express()
const PORT = 3000

app.get('/', (_, res) => 
    res.send('Software Avanzado - Tarea Práctica 5 - 201800709 - Juan Pablo Estrada Alemán')
)

app.listen(PORT, () => 
    console.log(`Server running on port: ${PORT}`)
);