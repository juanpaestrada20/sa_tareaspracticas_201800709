terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.78.0"
    }
  }
}

provider "google" {
  credentials = file("credentials.json")

  project = "sa-g1-343004"
  region  = "us-central1"
  zone    = "us-central1-a"
}

resource "random_id" "tp5_201800709" {
  byte_length = 8
}

resource "google_compute_instance" "default" {
  name         = "vm1-tp5-${random_id.tp5_201800709.hex}"
  machine_type = "e2-medium"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  metadata_startup_script = "sudo apt-get update; sudo apt-get dist-upgrade -y; curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -; sudo apt-get update; sudo apt install nodejs; cd /home/juanpa200799; git clone https://juanpaestrada20:Juanpaest20@gitlab.com/juanpaestrada20/basic_server.git; cd basic_server; sudo npm install; sudo node index.js"

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    ssh-key = "juanpa200799:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "google_compute_firewall" "default" {
  name    = "tp5-201800709-3000-22"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["3000", "22"]
  }
}

output "ip" {
  value = google_compute_instance.default.network_interface.0.access_config.0.nat_ip
}
